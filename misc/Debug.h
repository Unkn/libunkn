/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef DEBUG_H
#define DEBUG_H

#ifndef NDEBUG

#include <string>

std::string printableBytes(const uint8_t *bytes, size_t len);

#endif

#endif
