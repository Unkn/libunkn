/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef NDEBUG

#include <fmt/format.h>

#include "Debug.h"

std::string printableBytes(const uint8_t *bytes, size_t len)
{
    std::string ret;
    for (size_t i = 0; i < len; ++i)
    {
        if (i > 0)
            ret += " ";
        ret += fmt::format("{:02X}", bytes[i]);
    }
    return ret;
}

#endif
