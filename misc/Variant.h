/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef VARIANT_H
#define VARIANT_H

#include <memory>

// Credit to https://github.com/hbristow/argparse for the idea
// ArgumentParser::Any as of commit 43edb8acf2135509590631a0ac4582e3741016bf
class Variant
{
    public:
        Variant() :
            data_(nullptr)
        {}

        Variant(const Variant & obj) :
            Variant()
        {
            *this = obj;
        }

        Variant(Variant && obj) :
            Variant()
        {
            *this = std::move(obj);
        }

        template<typename ValueType>
        Variant(const ValueType & obj) :
            Variant()
        {
            *this = obj;
        }

        template<typename ValueType>
        Variant(ValueType && obj) :
            Variant()
        {
            *this = std::move(obj);
        }

        Variant & operator=(const Variant & obj)
        {
            if (this != &obj)
                data_.reset(obj.data_ ? obj.data_->clone() : nullptr);
            return *this;
        }

        Variant & operator=(Variant && obj)
        {
            if (this != &obj)
                data_ = std::move(obj.data_);
            return *this;
        }

        template<typename ValueType>
        Variant & operator=(const ValueType & obj)
        {
            data_.reset(static_cast<BaseHolder *>(new Holder<ValueType>(obj)));
            return *this;
        }

        template<typename ValueType>
        Variant & operator=(ValueType && obj)
        {
            data_.reset(static_cast<BaseHolder *>(new Holder<ValueType>(std::move(obj))));
            return *this;
        }

        bool valid() const
        {
            return data_ != nullptr;
        }

        template<typename ValueType>
        bool is() const
        {
            // TODO Verify that == on type_info is a 1 to 1 relationship with
            // types. type_info::hash_code can have collisions.
            return data_ != nullptr && data_->type_info() == typeid(ValueType);
        }

        template<typename ValueType>
        ValueType & as()
        {
            assert(is<ValueType>);
            return static_cast<Holder<ValueType> >(data_.get()).value_;
        }

        template<typename ValueType>
        const ValueType & as() const
        {
            assert(is<ValueType>);
            return static_cast<const Holder<ValueType> >(data_.get()).value_;
        }

    private:
        struct BaseHolder
        {
            virtual ~BaseHolder() {}
            virtual const std::type_info & type_info() const = 0;
            virtual BaseHolder * clone() const = 0;
        };

        template <typename ValueType>
        struct Holder : BaseHolder
        {
            Holder(const ValueType & value) :
                value_(value)
            {}

            Holder(ValueType && value) :
                value_(std::move(value))
            {}

            virtual const std::type_info & type_info() const
            {
                return typeid(ValueType);
            }

            virtual BaseHolder* clone() const
            {
                return new Holder(value_);
            }

            ValueType value_;
        };

        std::unique_ptr<BaseHolder> data_;
};

#endif
