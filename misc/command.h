/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef COMMAND_H
#define COMMAND_H

template<typename IdType, typename... Args>
class command
{
    public:
        virtual IdType id() const = 0;
        virtual bool process(Args... args) = 0;
};

#endif
