/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MATH_H
#define MATH_H

#include <cstdint>
#include <climits>
#include <cassert>

namespace Math
{
    template<typename IntType>
    static inline unsigned int rotbits(unsigned int n)
    {
        return n % (CHAR_BIT * sizeof(IntType));
    }

    /**
     * @warning Be careful about any operation on the input integer so that the
     * bit length is as intended. The template will not be shy about any
     * implicit casting to fuck you up.
     *
     * @note This should be inlined to a rol instruction
     */
    template<typename IntType>
    static inline IntType rotl(IntType i, unsigned int n)
    {
        // 8 * size - 1
        const unsigned int mask = (CHAR_BIT * sizeof(i) - 1);
        // No point in rotating beyond the bitlength... can just use mod
        // bitlength to reduce n
        assert(n <= mask);

        n &= mask;
        return (i << n) | (i >> ((-n) & mask));
    }

    /**
     * @warning Be careful about any operation on the input integer so that the
     * bit length is as intended. The template will not be shy about any
     * implicit casting to fuck you up.
     *
     * @note This should be inlined to a ror instruction
     */
    template<typename IntType>
    static inline IntType rotr(IntType i, unsigned int n)
    {
        // 8 * size - 1
        const unsigned int mask = (CHAR_BIT * sizeof(i) - 1);
        // No point in rotating beyond the bitlength... can just use mod
        // bitlength to reduce n
        assert(n <= mask);

        n &= mask;
        return (i >> n) | (i << ((-n) & mask));
    }
}

#endif
