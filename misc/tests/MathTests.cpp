/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <gtest/gtest.h>

#include "Math.h"

template<typename IntType>
struct RotInput
{
    RotInput(IntType e, IntType i, unsigned int n) :
        e_(e), i_(i), n_(n)
    {}

    IntType e_;
    IntType i_;
    unsigned int n_;
};

using Rot8Input = RotInput<uint8_t>;
using Rot16Input = RotInput<uint16_t>;
using Rot32Input = RotInput<uint32_t>;

// Header maybe :|
template<typename IntType>
class rotlTests : public testing::TestWithParam<RotInput<IntType> >
{
};

#define ROTL_TEST(bitlen) \
using rotl ## bitlen ## Tests = rotlTests<uint ## bitlen ## _t>; \
TEST_P(rotl ## bitlen ## Tests, CorrectRotate) \
{\
    auto & t = GetParam();\
    ASSERT_EQ(t.e_, Math::rotl(t.i_, t.n_));\
}

ROTL_TEST(8)
ROTL_TEST(16)
ROTL_TEST(32)

INSTANTIATE_TEST_CASE_P(rotl8, rotl8Tests, testing::Values(
            Rot8Input{ 0xF0, 0x0F, 4 },
            Rot8Input{ 0x0F, 0xF0, 4 },
            Rot8Input{ 0x01, 0x80, 1 },
            Rot8Input{ 0x40, 0x80, 7 }
            ));
// TODO bitlen 16 and 32


// Header maybe :|
template<typename IntType>
class rotrTests : public testing::TestWithParam<RotInput<IntType> >
{
};

#define ROTR_TEST(bitlen) \
using rotr ## bitlen ## Tests = rotrTests<uint ## bitlen ## _t>; \
TEST_P(rotr ## bitlen ## Tests, CorrectRotate) \
{\
    auto & t = GetParam();\
    ASSERT_EQ(t.e_, Math::rotr(t.i_, t.n_));\
}

ROTR_TEST(8)
ROTR_TEST(16)
ROTR_TEST(32)

INSTANTIATE_TEST_CASE_P(rotr8, rotr8Tests, testing::Values(
            Rot8Input{ 0xF0, 0x0F, 4 },
            Rot8Input{ 0x0F, 0xF0, 4 },
            Rot8Input{ 0x80, 0x01, 1 },
            Rot8Input{ 0x80, 0x40, 7 }
            ));
// TODO bitlen 16 and 32
