/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef COMMAND_ROUTER_H
#define COMMAND_ROUTER_H

#include <memory>
#include <map>

#include "command.h"

template<typename IdType, typename... Args>
class command_router
{
    public:
        bool register_command(std::unique_ptr<command<IdType, Args...> > && cmd)
        {
            auto id = cmd->id();
            auto it = commands_.find(id);
            if (it != commands_.end())
                return false;
            commands_.emplace(id, std::move(cmd));
            return true;
        }

        virtual bool route(const IdType & id, Args... args)
        {
            auto it = commands_.find(id);
            return it != commands_.end() ? it->second->process(args...) : false;
        }

    private:
        std::map<IdType, std::unique_ptr<command<IdType, Args...> > > commands_;
};

#endif
