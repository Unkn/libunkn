/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MYSQLLAZYBIND_H
#define MYSQLLAZYBIND_H

#include <mysql/mysql.h>
#include <cstring>
#include <cstdint>
#include <cassert>

struct MySQLLazyBind
{
    virtual ~MySQLLazyBind() { }
    virtual unsigned int count() const = 0;
    virtual MYSQL_BIND * bindings() = 0;
};

template<unsigned int Count>
struct LazyBind : MySQLLazyBind
{
    LazyBind()
    {
        idx_ = -1;
        memset(bindings_, 0, sizeof(bindings_));

        for (unsigned int i = 0; i < Count; ++i)
        {
            bindings_[i].length = length_ + i;
        }
    }
    virtual ~LazyBind() { }

    virtual unsigned int count() const { return Count; }
    virtual MYSQL_BIND * bindings() { return bindings_; }

    void set_index(unsigned int index)
    {
        assert(0 <= index && index < Count);
        // First thing we do in bind is increment
        idx_ = index - 1;
    }

    void bind(enum enum_field_types type, char * data, unsigned long length = 0)
    {
        ++idx_;
        assert(0 <= idx_ && idx_ < Count);

        auto & b = bindings_[idx_];

        b.buffer_type = type;
        b.buffer = data;
        b.buffer_length = length;

        bind_internal(b, type, data, length);
    }

    void set_unsigned(unsigned int index)
    {
        assert(0 <= index && index < Count);
        bindings_[index].is_unsigned = 1;
    }

    virtual void bind_internal(
            MYSQL_BIND & binding __attribute__ ((unused)),
            enum enum_field_types type __attribute__ ((unused)),
            char * data __attribute__ ((unused)),
            unsigned long length __attribute__ ((unused)))
    {
        // Pineapple
    }

    void bind(int8_t & data)
    {
        bind(MYSQL_TYPE_TINY, reinterpret_cast<char *>(&data));
    }

    void bind(uint8_t & data)
    {
        bind(MYSQL_TYPE_TINY, reinterpret_cast<char *>(&data));
        set_unsigned(idx_);
    }

    void bind(int16_t & data)
    {
        bind(MYSQL_TYPE_SHORT, reinterpret_cast<char *>(&data));
    }

    void bind(uint16_t & data)
    {
        bind(MYSQL_TYPE_SHORT, reinterpret_cast<char *>(&data));
        set_unsigned(idx_);
    }

    void bind(int32_t & data)
    {
        bind(MYSQL_TYPE_LONG, reinterpret_cast<char *>(&data));
    }

    void bind(uint32_t & data)
    {
        bind(MYSQL_TYPE_LONG, reinterpret_cast<char *>(&data));
        set_unsigned(idx_);
    }

    void bind(int64_t & data)
    {
        bind(MYSQL_TYPE_LONGLONG, reinterpret_cast<char *>(&data));
    }

    void bind(uint64_t & data)
    {
        bind(MYSQL_TYPE_LONGLONG, reinterpret_cast<char *>(&data));
        set_unsigned(idx_);
    }

    void bind(uint8_t * data, unsigned long length)
    {
        bind(MYSQL_TYPE_BLOB, reinterpret_cast<char *>(data), length);
    }

    void bind(char * data, unsigned long length)
    {
        bind(MYSQL_TYPE_STRING, data, length);
    }

    // The MYSQL_BINDs need to be in contiguous memory
    MYSQL_BIND bindings_[Count];
    unsigned long length_[Count];
    unsigned int idx_;
};

template<unsigned int Count>
struct LazyQueryBind : LazyBind<Count>
{
    LazyQueryBind() :
        LazyBind<Count>()
    {

    }
    virtual ~LazyQueryBind() { }

    virtual void bind_internal(
            MYSQL_BIND & binding,
            enum enum_field_types type,
            char * data __attribute__ ((unused)),
            unsigned long length)
    {
        if (type == MYSQL_TYPE_BLOB || type == MYSQL_TYPE_STRING)
            *binding.length = length;
    }
};

template<unsigned int Count>
struct LazyResultBind : LazyBind<Count>
{
    LazyResultBind() :
        LazyBind<Count>()
    {
        for (unsigned int i = 0; i < Count; ++i)
        {
            // Why do we need to specify parent here :| ?
            this->bindings_[i].is_null = is_null_ + i;
            this->bindings_[i].error = error_ + i;
        }
    }
    virtual ~LazyResultBind() { }

    my_bool is_null_[Count];
    my_bool error_[Count];
};

#endif
