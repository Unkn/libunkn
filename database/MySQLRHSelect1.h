/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MYSQLRHSELECT1_H
#define MYSQLRHSELECT1_H

#include <cstdint>

#include "MySQLResultHandler.h"
#include "MySQLLazyBind.h"

class MySQLRHSelect1 : public MySQLResultHandler
{
    public:
        MySQLRHSelect1();
        virtual ~MySQLRHSelect1();

        virtual MYSQL_BIND * getBindings();
        virtual unsigned int getBindingCount() const;
        virtual bool processRow();

        bool processed() const;

    private:
        bool processed_;
        int32_t i_;
        LazyResultBind<1> lazy_;
};

#endif
