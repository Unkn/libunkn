find_package(MySQL)

if(MySQL_FOUND)
    option(USE_C_API_TRANSACTION "Uses the MySQL C api for performing transactions instead of raw queries" OFF)

    add_library(database
        MySQLConnection.cpp
        MySQLRHSelect1.cpp)
    target_link_libraries(database PUBLIC
        logger
        misc
        buffer-cxx
        ${MYSQL_LIBRARY})
    if(${USE_C_API_TRANSACTION})
        target_compile_definitions(ms_server PUBLIC -DUSE_MYSQL_C_TRANSACTION_API=1)
    endif()
    target_include_directories(database PUBLIC ./)
    set_property(TARGET database PROPERTY CXX_STANDARD 17)
endif()
