/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#ifndef MYSQLCONNECTION_H
#define MYSQLCONNECTION_H

#include <mysql/mysql.h>
#include <string_view>
#include <memory>

#include "MySQLResultHandler.h"
#include "MySQLLazyBind.h"

struct mysql_stmt_deleter
{
    mysql_stmt_deleter() { }
    mysql_stmt_deleter(const mysql_stmt_deleter &) { }
    mysql_stmt_deleter(mysql_stmt_deleter &) { }
    mysql_stmt_deleter(mysql_stmt_deleter &&) { }

    void operator()(MYSQL_STMT * stmt) const
    {
        if (stmt)
            mysql_stmt_close(stmt);
    }
};

using MySQLStmt = std::unique_ptr<MYSQL_STMT, mysql_stmt_deleter>;

class MySQLConnection
{
    public:
        MySQLConnection();
        virtual ~MySQLConnection();

        virtual bool connect(const char * host, const char * user, const char * passwd, const char * db);
        // TODO API for results
        virtual MySQLStmt prepare(const std::string & stmt, MySQLLazyBind & args);
        virtual bool exec(const std::string & stmt, MySQLLazyBind & args, int * affected_rows = nullptr);
        virtual bool query(const std::string & stmt, MySQLLazyBind & args, MySQLResultHandler & rh);

        virtual bool exec(MySQLStmt & stmt);
        virtual bool exec(const std::string & stmt);
        virtual bool exec(const std::string & stmt, MYSQL_BIND * args, unsigned int argCount);
        virtual bool query(const std::string & stmt, MYSQL_BIND * args, unsigned int argCount, MySQLResultHandler & rh);

        void beginTransaction();
        void endTransaction(bool commit = true);

        MYSQL mysql_;

    private:
        bool queryInternal(MYSQL_STMT *& prep, const std::string & stmt, MYSQL_BIND * args, unsigned int argCount);
        bool process_results(MYSQL_STMT * prep, MySQLResultHandler & rh);
};

#include <cassert>

// TODO Remove all this shit
template<typename T>
void bindIt(MYSQL_BIND & bind __attribute__ ((unused)), T & thing __attribute__ ((unused)), unsigned long thingCap __attribute__ ((unused)), unsigned long & thingLen __attribute__ ((unused)))
{
    assert(!"fuck off");
}

template<typename T>
void bindIt(MYSQL_BIND & bind __attribute__ ((unused)), T & thing __attribute__ ((unused)))
{
    assert(!"fuck off");
}

template<>
void bindIt(MYSQL_BIND & bind, int8_t & thing);

template<>
void bindIt(MYSQL_BIND & bind, int16_t & thing);

template<>
void bindIt(MYSQL_BIND & bind, int32_t & thing);

template<>
void bindIt(MYSQL_BIND & bind, int64_t & thing);

template<>
void bindIt(MYSQL_BIND & bind, uint8_t & thing);

template<>
void bindIt(MYSQL_BIND & bind, uint16_t & thing);

template<>
void bindIt(MYSQL_BIND & bind, uint32_t & thing);

template<>
void bindIt(MYSQL_BIND & bind, uint64_t & thing);

template<>
void bindIt(MYSQL_BIND & bind, std::string_view & s, unsigned long thingCap, unsigned long & thingLen);

#endif
