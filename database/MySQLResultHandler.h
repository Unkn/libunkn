/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#ifndef MYSQLRESULTHANDLER_H
#define MYSQLRESULTHANDLER_H

#include <mysql/mysql.h>

class MySQLResultHandler
{
    public:
        virtual MYSQL_BIND * getBindings() = 0;
        virtual unsigned int getBindingCount() const = 0;
        virtual bool processRow() = 0;
};

#endif
