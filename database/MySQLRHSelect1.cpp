/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MySQLRHSelect1.h"

#include "Logger.h"

MySQLRHSelect1::MySQLRHSelect1() :
    processed_(false),
    i_(0),
    lazy_()
{
}

MySQLRHSelect1::~MySQLRHSelect1()
{
    // Pineapple
}

MYSQL_BIND * MySQLRHSelect1::getBindings()
{
    return lazy_.bindings_;
}

unsigned int MySQLRHSelect1::getBindingCount() const
{
    return lazy_.count();
}

bool MySQLRHSelect1::processRow()
{
    if (processed_)
    {
        LOG_DEBUG("Select 1 result handler processed more than 1 row");
        return false;
    }
    else if (i_ != 1)
    {
        LOG_DEBUG("Select 1 result handler processed an invalid result");
        return false;
    }

    processed_ = true;
    return true;
}

bool MySQLRHSelect1::processed() const
{
    return processed_;
}
