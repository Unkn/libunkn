/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#include <cassert>
#include <cstring>

#include "MySQLConnection.h"
#include "Logger.h"

MySQLConnection::MySQLConnection()
{
    // TODO Either add an interface that MySQLConnection inherits from for unit
    // tests with dependency injection or don't do this here...
    if (!mysql_init(&mysql_))
        // TODO Just use a generic exception to add the message?
        throw std::bad_alloc(/*"Failed to allocate memory for new MySQL connection"*/);
}

MySQLConnection::~MySQLConnection()
{
    mysql_close(&mysql_);
}

/**
 * @warning passwd should be zeroized by the caller
 *
 * @param host
 * @param user
 * @param passwd
 * @param db
 *
 * @return 
 */
bool MySQLConnection::connect(const char * host, const char * user, const char * passwd, const char * db)
{
    bool ret = mysql_real_connect(&mysql_, host, user, passwd, db, 0, nullptr, 0);
    if (!ret)
        LOG_ERROR("Failed to connect to database '{}' at {}: {}", db, host, mysql_error(&mysql_));
    return ret;
}

// TODO Make the rest of the code use this
MySQLStmt MySQLConnection::prepare(
        const std::string & stmt,
        MySQLLazyBind & args)
{
    MYSQL_STMT * prep = nullptr;
    mysql_stmt_deleter d;

    if (!(prep = mysql_stmt_init(&mysql_)))
        LOG_DEBUG("Failed to init prepared statement");
    else if (mysql_stmt_prepare(prep, stmt.c_str(), stmt.size()) != 0)
        LOG_DEBUG("Failed to prepare statement [{}]: {}", stmt, mysql_stmt_error(prep));
    else if (assert(args.count() == mysql_stmt_param_count(prep)),
            mysql_stmt_bind_param(prep, args.bindings()) != 0)
        LOG_DEBUG("Failed to bind prepare statement args: {}", mysql_stmt_error(prep));

    return MySQLStmt(prep, d);
}

bool MySQLConnection::exec(const std::string & stmt, MySQLLazyBind & args, int * affected_rows)
{
    bool ret = false;
    auto prep = prepare(stmt, args);
    if (prep)
    {
        ret = exec(prep);
        if (ret && affected_rows)
            *affected_rows = mysql_stmt_affected_rows(prep.get());
    }
    return ret;
}

bool MySQLConnection::query(
        const std::string & stmt,
        MySQLLazyBind & args,
        MySQLResultHandler & rh)
{
    bool ret = false;
    auto prep = prepare(stmt, args);
    // TODO Logging
    if (prep)
        ret = exec(prep) && process_results(prep.get(), rh);
    return ret;
}

bool MySQLConnection::exec(MySQLStmt & stmt)
{
    bool ret = true;
    auto prep = stmt.get();
    if (mysql_stmt_execute(prep) != 0)
    {
        LOG_DEBUG("Failed to execute prepare statement: {}", mysql_stmt_error(prep));
        ret = false;
    }
    return ret;
}

bool MySQLConnection::exec(const std::string & stmt)
{
    int err = mysql_query(&mysql_, stmt.c_str());
    if (err != 0)
        LOG_DEBUG("Failed to execute query [{}] ({}): {}", stmt, err, mysql_error(&mysql_));
    return err == 0;
}

bool MySQLConnection::exec(const std::string & stmt, MYSQL_BIND * args, unsigned int argCount)
{
    MYSQL_STMT * prep = nullptr;

    bool ret = queryInternal(prep, stmt, args, argCount);

    if (prep && mysql_stmt_close(prep) != 0)
        LOG_DEBUG("Failed to release prepared statement");

    return ret;
}

bool MySQLConnection::query(
        const std::string & stmt,
        MYSQL_BIND * args,
        unsigned int argCount,
        MySQLResultHandler & rh)
{
    MYSQL_STMT * prep = nullptr;
    bool ret = false;

    // TODO Might want to have the resultant column count verified against the
    // total columns expected by the result handler
    if (!queryInternal(prep, stmt, args, argCount))
        ; // TODO log
    else if (!process_results(prep, rh))
        ; // TODO log
    else
        ret = true;

    if (prep && mysql_stmt_close(prep) != 0)
        LOG_DEBUG("Failed to release prepared statement");

    return ret;
}

// TODO Should probably ret success/failure
void MySQLConnection::beginTransaction()
{
#ifdef USE_MYSQL_C_TRANSACTION_API
    if (mysql_autocommit(&mysql_, 0) != 0)
        LOG_DEBUG("Error disabling autocommit");
#else
    if (!exec("START TRANSACTION"))
        LOG_DEBUG("Failed to begin transaction");
#endif
}

// TODO Should probably ret success/failure
void MySQLConnection::endTransaction(bool commit)
{
#ifdef USE_MYSQL_C_TRANSACTION_API
    if ((commit ? mysql_commit(&mysql_) : mysql_rollback(&mysql_)) != 0)
        LOG_DEBUG("Transaction {} error", commit ? "commit" : "rollback");

    if (mysql_autocommit(&mysql_, 1) != 0)
        LOG_DEBUG("Error enabling autocommit");
#else
    if (!exec(commit ? "COMMIT" : "ROLLBACK"))
        LOG_DEBUG("Failed to begin transaction");
#endif
}

bool MySQLConnection::queryInternal(MYSQL_STMT *& prep, const std::string & stmt, MYSQL_BIND * args, unsigned int argCount)
{
    bool ret = false;

    assert(args != nullptr);
    assert(argCount > 0);

    if (!(prep = mysql_stmt_init(&mysql_)))
        LOG_DEBUG("Failed to init prepared statement");
    else if (mysql_stmt_prepare(prep, stmt.c_str(), stmt.size()) != 0)
        LOG_DEBUG("Failed to prepare statement [{}]: {}", stmt, mysql_stmt_error(prep));
    else if (assert(argCount == mysql_stmt_param_count(prep)),
            mysql_stmt_bind_param(prep, args) != 0)
        LOG_DEBUG("Failed to bind prepare statement args: {}", mysql_stmt_error(prep));
    else if (mysql_stmt_execute(prep) != 0)
        LOG_DEBUG("Failed to execute prepare statement: {}", mysql_stmt_error(prep));
    else
        ret = true;

    return ret;
}

bool MySQLConnection::process_results(
        MYSQL_STMT * prep,
        MySQLResultHandler & rh)
{
    assert(prep != nullptr);

    bool ret = false;

    if (mysql_stmt_bind_result(prep, rh.getBindings()) != 0)
        LOG_DEBUG("Failed to bind result set to prepared statement");
    else
    {
#define MYSQL_DATA_PROCESS_ERROR (-1)
        // TODO Bad but lazy :| just makin sure it's unused for MySQL 5.7
        static_assert(MYSQL_DATA_PROCESS_ERROR != MYSQL_NO_DATA && MYSQL_DATA_PROCESS_ERROR != MYSQL_DATA_TRUNCATED);

        int status;
        while ((status = mysql_stmt_fetch(prep)) == 0)
        {
            if (!rh.processRow())
            {
                status = MYSQL_DATA_PROCESS_ERROR;
                break;
            }
        }

        switch (status)
        {
            case MYSQL_NO_DATA:
                ret = true;
                break;

            case MYSQL_DATA_TRUNCATED:
                LOG_DEBUG("Data truncation occured when fetching row data");
                break;

            case MYSQL_DATA_PROCESS_ERROR:
                LOG_DEBUG("Result handler failed to process row data");
                break;

            case 1:
                LOG_DEBUG("Failed to fetch row data: {}", mysql_stmt_error(prep));
                break;

            default:
                assert(!"mystery status");
                break;
        }
#undef MYSQL_DATA_PROCESS_ERROR
    }

    return ret;
}

// TODO Get rid of this shit
template<>
void bindIt(MYSQL_BIND & bind, int8_t & thing)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_TINY;
    bind.buffer = &thing;
}

template<>
void bindIt(MYSQL_BIND & bind, int16_t & thing)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_SHORT;
    bind.buffer = &thing;
}

template<>
void bindIt(MYSQL_BIND & bind, int32_t & thing)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_LONG;
    bind.buffer = &thing;
}

template<>
void bindIt(MYSQL_BIND & bind, int64_t & thing)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_LONGLONG;
    bind.buffer = &thing;
}

template<>
void bindIt(MYSQL_BIND & bind, uint8_t & thing)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_TINY;
    bind.buffer = &thing;
    bind.is_unsigned = 1;
}

template<>
void bindIt(MYSQL_BIND & bind, uint16_t & thing)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_SHORT;
    bind.buffer = &thing;
    bind.is_unsigned = 1;
}

template<>
void bindIt(MYSQL_BIND & bind, uint32_t & thing)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_LONG;
    bind.buffer = &thing;
    bind.is_unsigned = 1;
}

template<>
void bindIt(MYSQL_BIND & bind, uint64_t & thing)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_LONGLONG;
    bind.buffer = &thing;
    bind.is_unsigned = 1;
}

template<>
void bindIt(MYSQL_BIND & bind, std::string_view & s, unsigned long thingCap, unsigned long & thingLen)
{
    memset(&bind, 0, sizeof(bind));
    bind.buffer_type = MYSQL_TYPE_STRING;
    // Have faith that MySQL wont fuck with it...
    bind.buffer = const_cast<char *>(s.data());
    bind.buffer_length = thingCap;
    bind.length = &thingLen;
}
