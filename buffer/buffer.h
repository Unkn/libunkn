/**
 * @file buffer.h
 * @author Alex Mardikian
 *
 * @license Mozilla Public License, v. 2.0 - http://mozilla.org/MPL/2.0/
 *
 *      This Source Code Form is subject to the terms of the Mozilla Public
 *      License, v. 2.0. If a copy of the MPL was not distributed with this
 *      file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *      The original source is available at
 *      https://github.com/amardikian/Unkn-Trainer-Template/
 *
 * @note Source of most of the code in this file is from the SQLite shell and
 *  has been modified and minimized to perform a more specialized task then the
 *  SQLite shell.
 */

#ifndef BUFFER_H
#define BUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

#define BUFFER_ERR_NONE 0

struct buffer
{
    size_t size;
    size_t capacity;
    uint8_t * data;
};
typedef struct buffer buffer;

buffer * buffer_new();
void buffer_free(buffer * buff);
uint8_t * buffer_release(buffer * buff);
buffer * buffer_copy(const buffer* buff);

void buffer_clear(buffer * buff);
int buffer_write(buffer * buff, const void * data, size_t len);
int buffer_expand(buffer * buff, size_t available);

#ifdef __cplusplus
}
#endif

#endif // #ifndef _BUFFER_H
