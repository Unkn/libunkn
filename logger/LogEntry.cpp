/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cassert>
#include <ctime>
#include <iomanip>

#include "LogEntry.h"

// TODO This function really belongs with the definition of LogLevel
std::string LogEntry::logLevelString() const
{
    const char *name = "";
    switch (level_)
    {
        case LogLevel::Emergency:
            name = "EMERGENCY";
            break;
        case LogLevel::Alert:
            name = "ALERT";
            break;
        case LogLevel::Critical:
            name = "CRITICAL";
            break;
        case LogLevel::Error:
            name = "ERROR";
            break;
        case LogLevel::Warning:
            name = "WARNING";
            break;
        case LogLevel::Notice:
            name = "NOTICE";
            break;
        case LogLevel::Informational:
            name = "INFO";
            break;
        case LogLevel::Debug:
            name = "DEBUG";
            break;

        default:
            assert(!"Invalid log level");
            break;
    }

    return name;
}

std::ostream & operator<<(std::ostream & os, const LogEntry & entry)
{
    os << std::put_time(std::gmtime(&entry.date_), "%F %T")
        << "|"
        << std::chrono::duration_cast<std::chrono::microseconds>(entry.monotonic_.time_since_epoch()).count() << "µs"
        << "|"
        << entry.logLevelString()
        << "|"
        << entry.message_;
    return os;
}
