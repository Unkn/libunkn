/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef STDOUTLOGMETHOD_H
#define STDOUTLOGMETHOD_H

#include "LogMethod.h"

#define USE_STDOUT_LOG_METHOD Logger::addLogMethod(std::move(std::unique_ptr<StdOutLogMethod>(new StdOutLogMethod)))

class StdOutLogMethod : public LogMethod
{
    public:
        StdOutLogMethod();
        virtual ~StdOutLogMethod();

        virtual void log(const std::string & id, const LogEntry & entry);
};

#endif
