/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FILELOGMETHOD_H
#define FILELOGMETHOD_H

#include <fstream>
#include <experimental/filesystem>

#include "LogMethod.h"

// TODO This class needs to be tested :|
class FileLogMethod : public LogMethod
{
    public:
        FileLogMethod();
        virtual ~FileLogMethod();

        bool setLogFile(const std::experimental::filesystem::v1::path &logFile);

        virtual void log(const LogEntry &entry);

    private:
        static const std::ofstream::pos_type MAX_LOG_SIZE;
        static const std::experimental::filesystem::v1::path EXTENSION;

        std::experimental::filesystem::v1::path logFile_;
        std::ofstream of_;

        bool openLogFile();

        std::experimental::filesystem::v1::path getCurrentLogFile() const;
        std::experimental::filesystem::v1::path getNextRotatedLogFile() const;

        bool isFull();
        bool rotateLogs();
        int getNextLogFileIndex() const;
};

#endif
