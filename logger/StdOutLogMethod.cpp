/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <iostream>

#include "StdOutLogMethod.h"

StdOutLogMethod::StdOutLogMethod()
{
}

StdOutLogMethod::~StdOutLogMethod()
{
}

void StdOutLogMethod::log(const std::string & id __attribute__ ((unused)), const LogEntry &entry)
{
    // Don't use endl, we dont want flushing to cause any delays
    std::cout << entry << "\n";
}
