add_library(network
    Socket.cpp
    LinuxSocket.cpp
    LinuxSocketFactory.cpp
    # TODO This will eventuall be split into OS specific sources
    network_utils.cpp)
target_link_libraries(network
    logger
    buffer-cxx
    misc)
target_include_directories(network PUBLIC ./)
set_property(TARGET network PROPERTY CXX_STANDARD 17)

add_library(network_logfree
    Socket.cpp
    LinuxSocket.cpp)
target_link_libraries(network_logfree
    buffer-cxx
    misc)
target_include_directories(network_logfree PUBLIC ./)
set_property(TARGET network_logfree PROPERTY CXX_STANDARD 17)
target_compile_definitions(network_logfree PRIVATE -DDISABLE_SOCKET_LOGGING)
