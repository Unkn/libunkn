/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "LinuxSocketFactory.h"


#include "Logger.h"
#include "network_utils.h"


// TODO I dont think we will need this anymore :|
std::unique_ptr<Socket> LinuxSocketFactory::connect_localhost(
        uint16_t port)
{
    int socket_fd = network_utils::connect_localhost_socket(port);
    return socket_fd >= 0 ? std::unique_ptr<Socket>(new LinuxSocket(socket_fd)) : nullptr;
}
