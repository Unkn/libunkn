/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef LINUXSOCKETFACTORY_H
#define LINUXSOCKETFACTORY_H

#include <memory>

#include "LinuxSocket.h"

namespace LinuxSocketFactory
{
    std::unique_ptr<Socket> connect_localhost(uint16_t port);
};

#endif
