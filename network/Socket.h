/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SOCKET_H
#define SOCKET_H

#include <cstddef>
#include <cstdint>
#include <memory>

#include "buffer.hpp"

class Socket
{
    public:
        // TODO I don't think I want to do the bind/connect here. Maybe a
        // factory for creating the socket that does that so only valid
        // "Socket"'s can exist.
#if 0
        virtual bool connect() = 0;
        virtual bool bind() = 0;
#endif

        /**
         * Send data
         *
         * @param data Data to send
         * @param len Length of data
         *
         * @return true on success
         */
        virtual bool send(const uint8_t * data, size_t len) = 0;

        /**
         * Send data
         *
         * @param buff Data to send
         *
         * @return true on success
         */
        virtual bool send(const Buffer & buff) = 0;

        /**
         * Receive data
         *
         * @param data Buffer to write recieved data to
         * @param[int,out] len Length of data buffer; length of message written to data
         *
         * @return true on success
         */
        virtual bool recv(uint8_t * data, size_t & len) = 0;

        /**
         * Receive data
         *
         * @note If the buffer's size is 0, will read until the read would
         * block. If the buffer's size is non-zero, then the the method will
         * read at most size bytes and resize the buffer to the total read bytes.
         *
         * @param buff Buffer to write received data to
         *
         * @return true on success
         */
        virtual bool recv(Buffer & buff) = 0;
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(Socket *) -> unique_ptr<Socket, default_delete<Socket> >;
}

#endif
