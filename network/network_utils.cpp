/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "network_utils.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "Logger.h"

// TODO Separate source files for OS specific things and use cmake to determine
// which source files get compiled

namespace network_utils
{
    int connect_socket(uint16_t port, uint32_t ip);
    int listen_socket(uint16_t port, uint32_t ip, int backlog);
}

int network_utils::connect_socket(uint16_t port, std::string_view ip)
{
    struct in_addr a;
    if (inet_pton(AF_INET, ip.data(), &a) != 1)
        return -1;
    return connect_socket(port, a.s_addr);
}

int network_utils::connect_localhost_socket(uint16_t port)
{
    return connect_socket(port, htonl(INADDR_LOOPBACK));
}

int network_utils::listen_socket(
        uint16_t port,
        std::string_view ip,
        int backlog)
{
    struct in_addr a;
    if (inet_pton(AF_INET, ip.data(), &a) != 1)
        return -1;
    return listen_socket(port, a.s_addr, backlog);
}

int network_utils::listen_socket_localhost(
        uint16_t port,
        int backlog)
{
    return listen_socket(port, htonl(INADDR_LOOPBACK), backlog);
}

int network_utils::listen_socket_any(
        uint16_t port,
        int backlog)
{
    return listen_socket(port, htonl(INADDR_ANY), backlog);
}

int network_utils::connect_socket(uint16_t port, uint32_t ip)
{
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        LOG_ERROR("Failed to create socket");
        return -1;
    }

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = ip;
    addr.sin_port = htons(port);

    if (connect(socket_fd, reinterpret_cast<struct sockaddr *>(&addr), sizeof(addr)) < 0)
    {
        LOG_ERROR("Failed to connect socket to localhost port {}", port);
        close(socket_fd);
        return -1;
    }

    return socket_fd;
}

int network_utils::listen_socket(uint16_t port, uint32_t ip, int backlog)
{
    bool ok = false;
    int listen_fd = -1;
    struct sockaddr_in a;

    memset(&a, 0, sizeof(a));

    a.sin_family = AF_INET;
    a.sin_addr.s_addr = ip;
    a.sin_port = htons(port);

    if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        fprintf(stderr, "Failed to create connection accept socket\n");
    else if (bind(listen_fd, (struct sockaddr *)&a, sizeof(a)) < 0)
        fprintf(stderr, "Failed to bind connection accept socket\n");
    // TODO Option for backlog
    else if (listen(listen_fd, backlog) < 0)
        fprintf(stderr, "Failed to listen on the accept socket\n");
    else
        ok = true;

    if (!ok && listen_fd >= 0)
    {
        close(listen_fd);
        listen_fd = -1;
    }

    return listen_fd;
}
