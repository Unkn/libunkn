/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cassert>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "LinuxSocket.h"
#ifndef DISABLE_SOCKET_LOGGING
#include "Logger.h"
#include "Debug.h"
#endif

LinuxSocket::LinuxSocket(int fd) :
    fd_(fd)
{
    assert(fd >= 0);
}

LinuxSocket::~LinuxSocket()
{
    close(fd_);
}

bool LinuxSocket::send(const uint8_t * data, size_t len)
{
    assert(data != nullptr);
    assert(len > 0);

    bool ret = false;
    // TODO Add an option to the socket to use MSG_DONTWAIT
    ssize_t i = ::send(fd_, data, len, 0);
    if (i < 0)
    {
#ifndef DISABLE_SOCKET_LOGGING
        LOG_WARN("Failed to send data to client {:d}", fd_);
#endif
    }
    else if (static_cast<size_t>(i) != len)
    {
#ifndef DISABLE_SOCKET_LOGGING
        LOG_WARN("Failed to send complete packet to client {:d}", fd_);
#endif
    }
    else
    {
        ret = true;
    }

#ifndef DISABLE_SOCKET_LOGGING
    LOG_DEBUG("SEND: ({:d}) {}", ret, printableBytes(data, len));
#endif

    return ret;
}

bool LinuxSocket::send(const Buffer & buff)
{
    return send(buff.data(), buff.size());
}

bool LinuxSocket::recv(uint8_t * data, size_t & len)
{
    assert(data != nullptr);
    assert(len > 0);

    bool ret = true;
    ssize_t i = ::read(fd_, data, len);
    if (i < 0)
    {
        // TODO May be a good idea to update the return to indicate if it was
        // blocked OR have an option in the socket object to specify whether or
        // not this should be considered an error
        if (errno == EWOULDBLOCK)
        {
#ifndef DISABLE_SOCKET_LOGGING
            LOG_WARN("Read from client {:d} would have blocked", fd_);
#endif
            len = 0;
        }
        else
        {
#ifndef DISABLE_SOCKET_LOGGING
            LOG_WARN("Failed to read data from client {:d}", fd_);
#endif
            ret = false;
        }
    }
    else
    {
        len = i;
    }
#ifndef DISABLE_SOCKET_LOGGING
    LOG_DEBUG("RECV: ({:d}) {}", ret, printableBytes(data, len));
#endif
    return ret;
}

bool LinuxSocket::recv(Buffer & buff)
{
    // If the size is nonzero, then we will grow until the buffer isn't filled.
    // Otherwise we will read up to size bytes.
    return buff.size() == 0 ? recvGrow(buff) : recvSingle(buff);
}

bool LinuxSocket::recvGrow(Buffer & buff)
{
    // Internally the buffer will do a doubling growth so this really just
    // needs to be any value > 0
    const int MIN_GROWTH = 128;
    size_t totalRecv = 0;
    size_t recvLen;
    bool ok = true;
    bool doRecv = true;

    if (buff.capacity() == 0)
        ok = buff.resize(MIN_GROWTH);

    do
    {
        recvLen = buff.capacity() - totalRecv;
        ok = recv(buff.data(), recvLen);
        if (ok)
        {
            // If the recvLen is 0 but recv reported success, then there is no
            // data available to be read
            if (recvLen == 0)
            {
                doRecv = false;
            }
            else
            {
                totalRecv += recvLen;
                if (totalRecv == buff.capacity())
                    ok = buff.expand(MIN_GROWTH);
            }
        }
    } while (ok && doRecv);

    if (ok)
        ok = buff.resize(totalRecv);

    return ok;
}

bool LinuxSocket::recvSingle(Buffer & buff)
{
    size_t len = buff.size();
    bool ok = recv(buff.data(), len);
    if (ok)
        buff.resize(len);
    return ok;
}
