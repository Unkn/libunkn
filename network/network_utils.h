/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef NETWORK_UTILS_H
#define NETWORK_UTILS_H

#include <string_view>

namespace network_utils
{
    int connect_socket(uint16_t port, std::string_view ip);
    int connect_localhost_socket(uint16_t port);

    int listen_socket(uint16_t port, std::string_view ip, int backlog);
    int listen_socket_localhost(uint16_t port, int backlog);
    int listen_socket_any(uint16_t port, int backlog);
}

#endif
