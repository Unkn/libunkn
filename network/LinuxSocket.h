/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef LINUXSOCKET_H
#define LINUXSOCKET_H

#include "Socket.h"

class LinuxSocket : public Socket
{
    public:
        LinuxSocket() = delete;
        LinuxSocket(int fd);
        virtual ~LinuxSocket();

        virtual bool send(const uint8_t * data, size_t len);
        virtual bool send(const Buffer & buff);

        virtual bool recv(uint8_t * data, size_t & len);
        virtual bool recv(Buffer & buff);

    private:
        int fd_;

        bool recvGrow(Buffer & buff);
        bool recvSingle(Buffer & buff);
};

#endif
