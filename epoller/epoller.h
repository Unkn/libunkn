/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef EPOLLER_H
#define EPOLLER_H

#include <memory>
#include <set>

#include "epoller_event.h"

class epoller
{
    public:
        epoller(int epoll_fd);
        virtual ~epoller();

        bool add_event(std::unique_ptr<epoller_event> && event);
        bool wait_and_process(int timeout = -1);

    private:
        int epoll_fd_;
        std::set<epoller_event *> events_;

        void remove_fd_from_epoll_set(int fd);
        void remove_event(epoller_event * event);
        void remove_event(decltype(events_)::iterator it);
        void clear_events();
};

#endif
