/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef EPOLLER_SIGINT_EVENT_H
#define EPOLLER_SIGINT_EVENT_H

#include "epoller_event.h"

class epoller_sigint_event : public epoller_event
{
    public:
        epoller_sigint_event();
        virtual ~epoller_sigint_event();

        virtual std::unique_ptr<epoller_job> process();
};

#endif
