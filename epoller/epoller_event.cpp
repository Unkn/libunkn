/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "epoller_event.h"

#include <cassert>
#include <unistd.h>

epoller_event::epoller_event(
        int fd,
        bool close) :
    fd_(fd),
    close_(close)
{
    assert(fd >= 0);
}

epoller_event::~epoller_event()
{
    if (close_)
        close(fd_);
}

int epoller_event::fd() const
{
    return fd_;
}
