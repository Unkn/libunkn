/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "epoller_job.h"

#ifndef EPOLLER_JOB_EXIT_H
#define EPOLLER_JOB_EXIT_H

class epoller_job_exit : public epoller_job
{
    public:
        epoller_job_exit() {}
        virtual ~epoller_job_exit() {}

        virtual type get_type() const { return type::exit; }
};

#endif
