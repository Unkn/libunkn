/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "epoller_sigint_event.h"

#include <csignal>
#include <cstdio>
#include <sys/eventfd.h>

#include <stdexcept>

#include "epoller_job_exit.h"

static int sigint_fd = -1;

static void signal_handler(int signal __attribute__ ((unused)))
{
    eventfd_write(sigint_fd, 1);
}

static int initalize_sigint_fd()
{
    if (sigint_fd >= 0)
    {
        fprintf(stderr, "Something else already initialized the SIGINT eventfd\n");
    }
    else if ((sigint_fd = eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK)) < 0)
    {
        fprintf(stderr, "Failed to create SIGINT event fd\n");
        sigint_fd = -1;
    }
    else
    {
        fprintf(stderr, "Registered SIGINT handler\n");
        std::signal(SIGINT, signal_handler);
    }

    return sigint_fd;
}

epoller_sigint_event::epoller_sigint_event() :
    epoller_event(initalize_sigint_fd())
{
    if (sigint_fd < 0)
        throw std::runtime_error("failed to initialize sigint fd");
}

epoller_sigint_event::~epoller_sigint_event()
{
    // Pineapple
}

std::unique_ptr<epoller_job> epoller_sigint_event::process()
{
    return std::make_unique<epoller_job_exit>();
}
