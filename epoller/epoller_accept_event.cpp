/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "epoller_accept_event.h"

#include <sys/socket.h>
#include <netinet/in.h>

#include "epoller_job_add.h"

epoller_accept_event::epoller_accept_event(
        int fd) :
    epoller_event(fd)
{
}

epoller_accept_event::~epoller_accept_event()
{
    // Pineapples
}

std::unique_ptr<epoller_job> epoller_accept_event::process()
{
    struct sockaddr_in info;
    socklen_t addr_len = sizeof(info);

    int accept_fd = accept4(fd(), reinterpret_cast<struct sockaddr *>(&info), &addr_len, SOCK_NONBLOCK | SOCK_CLOEXEC);
    if (accept_fd < 0)
    {
        fprintf(stderr, "Error accepting new client connection\n");
        return nullptr;
    }

    auto client = new_client(accept_fd, info);
    return client ? std::make_unique<epoller_job_add>(std::move(client)) : nullptr;
}

