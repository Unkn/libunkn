/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "epoller_factory.h"

#include <sys/epoll.h>

std::unique_ptr<epoller> epoller_factory::create()
{
    int epoll_fd = -1;
    if ((epoll_fd = epoll_create1(0)) < 0)
    {
        fprintf(stderr, "Failed to create epoll fd\n");
        return nullptr;
    }
    return std::make_unique<epoller>(epoll_fd);
}
