/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef EPOLLER_ACCEPT_EVENT_H
#define EPOLLER_ACCEPT_EVENT_H

#include <sys/socket.h>

#include "epoller_event.h"

class epoller_accept_event : public epoller_event
{
    public:
        epoller_accept_event(int fd);
        virtual ~epoller_accept_event();

        virtual std::unique_ptr<epoller_job> process();
        virtual std::unique_ptr<epoller_event> new_client(int accept_fd, const struct sockaddr_in & info) = 0;
};

#endif
