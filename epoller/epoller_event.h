/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef EPOLLER_EVENT_H
#define EPOLLER_EVENT_H

#include <memory>

#include "epoller_job.h"

class epoller_event
{
    public:
        epoller_event(int fd, bool close = true);
        virtual ~epoller_event();

        virtual std::unique_ptr<epoller_job> process() = 0;

        int fd() const;

    private:
        int fd_;
        bool close_;
};

#endif
