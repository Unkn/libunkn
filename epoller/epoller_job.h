/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef EPOLLER_JOB_H
#define EPOLLER_JOB_H

class epoller_job
{
    public:
        enum class type
        {
            add,
            del,
            exit
        };

        virtual ~epoller_job() {}

        virtual type get_type() const = 0;
};

#endif
