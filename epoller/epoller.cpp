/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "epoller.h"

#include <sys/epoll.h>
#include <cassert>

#include "epoller_job_add.h"

epoller::epoller(
        int epoll_fd) :
    epoll_fd_(epoll_fd)
{
}

epoller::~epoller()
{
    clear_events();
}

bool epoller::add_event(
        std::unique_ptr<epoller_event> && event)
{
    bool ok = false;
    int fd = event->fd();
    struct epoll_event ev;

    // TODO Specify the events in the epoller_event object
    ev.events = EPOLLIN;
    ev.data.ptr = event.get();

    if (epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, fd, &ev) < 0)
    {
        perror("epoll_ctl EPOLL_CTL_ADD");
        fprintf(stderr, "Failed to add listening socket to epoll\n");
    }
    else
    {
        ok = true;
        events_.emplace(event.release());
    }
    return ok;
}

bool epoller::wait_and_process(
        int timeout)
{
    bool run = true;
    // TODO Configurable
    const int max_events = 10;
    struct epoll_event events[max_events];

    int nfds = epoll_wait(epoll_fd_, events, max_events, timeout);
    if (nfds < 0)
    {
        fprintf(stderr, "epoll_wait call failure\n");
    }

    for (int i = 0; run && i < nfds; ++i)
    {
        assert(events[i].data.ptr != nullptr);
        auto event = static_cast<epoller_event *>(events[i].data.ptr);
        auto job = event->process();

        if (job)
        {
            switch (job->get_type())
            {
                case epoller_job::type::add:
                    add_event(std::move(static_cast<epoller_job_add *>(job.get())->event_));
                    break;

                case epoller_job::type::del:
                    remove_event(event);
                    break;

                case epoller_job::type::exit:
                    run = false;
                    break;

                default:
                    assert(!"Invalid job type");
                    break;
            }
        }
    }

    // TODO Idle check/kick option

    return run;
}

void epoller::remove_fd_from_epoll_set(
        int fd)
{
    if (epoll_ctl(epoll_fd_, EPOLL_CTL_DEL, fd, nullptr) < 0)
        perror("epoll_ctl");
}

void epoller::remove_event(
        epoller_event * event)
{
    assert(event != nullptr);
    auto it = events_.find(event);
    assert(it != events_.end());
    remove_event(it);
}

void epoller::remove_event(
        decltype(events_)::iterator it)
{
    auto event = *it;
    remove_fd_from_epoll_set(event->fd());
    events_.erase(it);
    delete event;
}

void epoller::clear_events()
{
    while (!events_.empty())
    {
        remove_event(events_.begin());
    }
}
