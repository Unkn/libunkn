/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef EPOLLER_JOB_ADD_H
#define EPOLLER_JOB_ADD_H

#include "epoller_job.h"
#include "epoller_event.h"

class epoller_job_add : public epoller_job
{
    public:
        epoller_job_add(
                std::unique_ptr<epoller_event> && event) :
            event_(std::move(event))
        {}
        virtual ~epoller_job_add() {}

        virtual type get_type() const { return type::add; }

        std::unique_ptr<epoller_event> event_;
};

#endif
