/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef EPOLLER_FACTORY_H
#define EPOLLER_FACTORY_H

#include <memory>
#include <string_view>

#include "epoller.h"

namespace epoller_factory
{
    std::unique_ptr<epoller> create();
}

#endif
