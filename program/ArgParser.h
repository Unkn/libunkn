/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef ARGPARSER_H
#define ARGPARSER_H

#include <map>
#include <memory>
#include <string_view>

#include "ArgOpt.h"

class ArgParser
{
    public:
        ArgParser();
        virtual ~ArgParser();

        void usage() const;
        bool parse(int argc, char **argv);

        bool addOpt(ArgOpt && opt);
        bool addOpts(std::vector<std::unique_ptr<ArgOpt> > && opts);

    protected:
        bool validate();

    private:
        std::vector<std::unique_ptr<ArgOpt> > values_;
        std::map<std::string_view, decltype(values_.size())> valueIndex_;

        bool argIsVal(int argc, char **argv, int idx) const;
};

#endif
