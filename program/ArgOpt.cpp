/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <algorithm>
#include <cctype>

#include "ArgOpt.h"

static Variant && basicParser(std::vector<const char *> args);

ArgOpt::Parser ArgOpt::BASIC_PARSER = basicParser;

ArgOpt::ArgOpt() :
    shortName_(),
    longName_(),
    desc_(),
    args_(NO_ARGUMENT),
    value_(),
    parser_(BASIC_PARSER)
{
}

ArgOpt::ArgOpt(const ArgOpt & obj) :
    ArgOpt()
{
    *this = obj;
}

ArgOpt::ArgOpt(ArgOpt && obj) :
    ArgOpt()
{
    *this = std::move(obj);
}

// TODO Document the exceptions
ArgOpt::ArgOpt(
        char shortName,
        int args,
        const Variant &value,
        std::string desc,
        Parser parser) :
    ArgOpt()
{
    try
    {
        setShortName(shortName);
        setArgs(args);
    }
    catch (std::exception & e)
    {
        throw e;
    }

    value_ = value;
    desc_ = desc;
    parser_ = parser;
}

// TODO Document the exceptions
ArgOpt::ArgOpt(
        const std::string & longName,
        int args,
        const Variant &value,
        std::string desc,
        Parser parser) :
    ArgOpt()
{
    try
    {
        setLongName(longName);
        setArgs(args);
    }
    catch (std::exception & e)
    {
        throw e;
    }

    value_ = value;
    desc_ = desc;
    parser_ = parser;
}

// TODO Document the exceptions
ArgOpt::ArgOpt(char shortName,
        const std::string & longName,
        int args,
        const Variant &value,
        std::string desc,
        Parser parser) :
    ArgOpt()
{
    try
    {
        setShortName(shortName);
        setLongName(longName);
        setArgs(args);
    }
    catch (std::exception & e)
    {
        throw e;
    }

    value_ = value;
    desc_ = desc;
    parser_ = parser;
}

ArgOpt::~ArgOpt()
{
    // Nothin
}

ArgOpt & ArgOpt::operator=(const ArgOpt & rhs)
{
    if (this != &rhs)
    {
        shortName_ = rhs.shortName_;
        longName_ = rhs.longName_;
        desc_ = rhs.desc_;
        args_ = rhs.args_;
        value_ = rhs.value_;
    }
    return *this;
}

ArgOpt & ArgOpt::operator=(ArgOpt && rhs)
{
    if (this != &rhs)
    {
        shortName_ = std::move(rhs.shortName_);
        longName_ = std::move(rhs.longName_);
        desc_ = std::move(rhs.desc_);
        args_ = rhs.args_;
        value_ = std::move(rhs.value_);
    }
    return *this;
}

const std::string &ArgOpt::getShortName() const
{
    return shortName_;
}

const std::string &ArgOpt::getLongName() const
{
    return longName_;
}

const std::string &ArgOpt::getDescription() const
{
    return desc_;
}

const Variant &ArgOpt::getValue() const
{
    return value_;
}

int ArgOpt::getArgs() const
{
    return args_;
}

bool ArgOpt::parse(std::vector<const char *> args)
{
    bool ok = false;
    // TODO Validate the arg count and shit
    if (ok)
        value_ = std::move(parser_(args));
    return ok;
}

void ArgOpt::setShortName(char c)
{
    if (!isalnum(c))
        throw std::invalid_argument("Short opt is required to be alphanumeric");

    shortName_ = "-";
    shortName_ += std::string(1, c);
}

void ArgOpt::setLongName(const std::string & s)
{
    if (s.length() < 2)
        throw std::invalid_argument("Long opt is required to be at least 2 characters");
    else if (s.front() == '_' || s.back() == '_')
        throw std::invalid_argument("Long opt cannot start or end with an underscore");
    else if (!std::all_of(s.begin(), s.end(), [](const char &c){ return c == '_' || isalnum(c); }))
        throw std::invalid_argument("Long opt can only consist of alphanumeric and underscore characters");

    longName_ = "--";
    longName_ += s;
}

void ArgOpt::setArgs(int i)
{
    if (i < -1)
        throw std::invalid_argument("Invalid number of expected arguments");

    args_ = i;
}

static Variant && basicParser(std::vector<const char *> args __attribute__ ((unused)))
{
    // TODO
    return std::move(Variant());
}
