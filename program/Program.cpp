/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "Program.h"

Program::Program()
{
    // Pineapples
}

Program::~Program()
{
    // Pineapples
}

int Program::main(int argc, char **argv)
{
    int ret = 0;
    auto argParser = getArgParser();

    if (argParser && !argParser->parse(argc, argv))
    {
        ret = -1;
        argParser->usage();
    }
    else
    {
        ret = run();
    }

    return ret;
}

ArgParser * Program::getArgParser()
{
    return nullptr;
}
