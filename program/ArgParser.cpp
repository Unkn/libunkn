/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "ArgParser.h"
#include "Logger.h"

ArgParser::ArgParser()
{
    // pineapples
}

ArgParser::~ArgParser()
{
    // pineapples
}

void ArgParser::usage() const
{
    // TODO
}

bool ArgParser::parse(
        int argc,
        char **argv)
{
    bool ok = true;

    for (int i = 0; ok && i < argc; ++i)
    {
        // NOTE Don't use getopt_long unless we porting it

        // TODO This is gonna create a string from the const char so its a
        // wasted alloc but who cares right now...
        auto it = valueIndex_.find(std::string_view(argv[i]));
        if (it == valueIndex_.end())
        {
            LOG_DEBUG("Processed invalid argument '{}'", argv[i]);
            ok = false;
            continue;
        }

        auto &argOpt = *values_[it->second];
        int argCount = argOpt.getArgs();
        std::vector<const char *> args;

        if (argCount == NO_ARGUMENT)
        {
            // Just passing through...
        }
        else if (argCount == OPTIONAL_ARGUMENT)
        {
            if (argIsVal(argc, argv, i + 1))
            {
                args.emplace_back(argv[i + 1]);
                ++i;
            }
        }
        else if (argCount == REQUIRED_ARGUMENT)
        {
            if (argIsVal(argc, argv, i + 1))
            {
                args.emplace_back(argv[i + 1]);
                ++i;
            }
            else
            {
                LOG_DEBUG("Required argument missing for option {}", argv[i]);
                ok = false;
            }
        }
        else
        {
            assert(!">1 arg not implemented");
            ok = false;
        }

        if (ok)
            ok = argOpt.parse(args);
    }

    if (ok && !validate())
    {
        LOG_DEBUG("Failed to validate command line arguments");
        ok = false;
    }

    return ok;
}

bool ArgParser::addOpt(ArgOpt && opt)
{
    std::vector<std::unique_ptr<ArgOpt> > opts;
    opts.emplace_back(std::make_unique<ArgOpt>(std::move(opt)));
    return addOpts(std::move(opts));
}

bool ArgParser::addOpts(std::vector<std::unique_ptr<ArgOpt> > && opts)
{
    bool ret = true;

    for (auto & opt : opts)
    {
        const std::string &sname = opt->getShortName();
        const std::string &lname = opt->getLongName();

        // TODO Maybe just assertions and remove the success return
        if (!sname.empty() && valueIndex_.find(sname) != valueIndex_.end())
        {
            ret = false;
            LOG_DEBUG("Short opt '{}' already specified", sname);
        }
        else if (!lname.empty() && valueIndex_.find(lname) != valueIndex_.end())
        {
            ret = false;
            LOG_DEBUG("Long opt '{}' already specified", lname);
            break;
        }

        if (!ret)
            break;

        if (!sname.empty())
            valueIndex_.emplace(sname, values_.size());
        if (!lname.empty())
            valueIndex_.emplace(lname, values_.size());
        values_.emplace_back(std::move(opt));
    }

    return ret;
}

bool ArgParser::validate()
{
    // By default no actual validation
    return true;
}

bool ArgParser::argIsVal(int argc, char **argv, int idx) const
{
    return idx < argc && *argv[idx] == '-';
}
