/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef ARGOPT_H
#define ARGOPT_H

#include <string>
#include <vector>
#include <functional>

#include "Variant.h"

#define NO_ARGUMENT 0
#define REQUIRED_ARGUMENT 1
#define OPTIONAL_ARGUMENT (-1)

class ArgOpt
{
    public:
        using Parser = std::function<Variant &&(std::vector<const char *>)>;

        ArgOpt(const ArgOpt & obj);
        ArgOpt(ArgOpt && obj);

        ArgOpt(char shortName, int args, const Variant &value, std::string desc, Parser parser = BASIC_PARSER);
        ArgOpt(const std::string & longName, int args, const Variant &value, std::string desc, Parser parser = BASIC_PARSER);
        ArgOpt(char shortName, const std::string & longName, int args, const Variant &value, std::string desc, Parser parser = BASIC_PARSER);

        virtual ~ArgOpt();

        ArgOpt & operator=(const ArgOpt & rhs);
        ArgOpt & operator=(ArgOpt && rhs);

        const std::string &getShortName() const;
        const std::string &getLongName() const;
        const std::string &getDescription() const;
        const Variant &getValue() const;
        int getArgs() const;

        bool parse(std::vector<const char *> args = {});

    private:
        ArgOpt();

        void setShortName(char c);
        void setLongName(const std::string &s);
        void setArgs(int i);

        std::string shortName_;
        std::string longName_;
        std::string desc_;
        int args_;
        Variant value_;
        Parser parser_;

        static Parser BASIC_PARSER;
};

#endif
