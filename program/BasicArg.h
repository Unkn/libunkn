/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef BASICARG_H
#define BASICARG_H

#include "OptArg.h"

class BasicArg : public OptArg
{
};

#endif
